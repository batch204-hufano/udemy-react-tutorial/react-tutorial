import React from "react";

const Person = (props) => {

    console.log(props)
    return(
        <>
            <p onClick={props.click}>I'm {props.name} and I am {props.age} year old! </p>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.name}/>
        </>
    )
}

export default Person;