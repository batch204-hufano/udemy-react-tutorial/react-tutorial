import './App.css';
import React, {useState} from 'react';

import Person from './Person/Person'
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

function App() {

  // React hooks
  const [name ,setName] = useState("Lexus")
  const [name2, setName2] = useState("June")
  const [age, setAge] = useState(15)

  const [userName, setUserName] = useState("useState")

  const switchNameHander = (name) => {
    console.log(name)
    // console.log("Was clicked!")
    setName(name)
    setName2("April")
    setAge(30)
  }

  const changedNameHandler = (e) => {
    setName2(e.target.value)
  }

  const changedUserNameHandler = (e) => {
    setUserName(e.target.value)
  }

  return (
    <div className="App">
      <Person name={name} age={age}/>
      <button onClick={() => switchNameHander('Juniee')}>Switch Name</button>
      <Person 
        click={() => switchNameHander("Boyy")}
        changed={changedNameHandler}
        name={name2}
        age={age}> My hobbies: Basketball
      </Person>
      <br></br>
      <br></br>
      <br></br>
      <UserInput value={userName} changed={changedUserNameHandler} />
      <UserOutput userName={userName}/>
      <UserOutput userName={userName}/>
      <UserOutput userName="dingAngBato"/>
    </div>
  );
}

export default App;
